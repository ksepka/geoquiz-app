package com.example.llama.geoquiz;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.content.Intent;

public class CheatActivity extends Activity {

    public static final String EXTRA_ANSWER_IS_TRUE = "com.example.llama.geoquiz.answer_is_true";
    public static final String EXTRA_ANSWER_SHOWN = "com.example.llama.geoquiz.answer_shown";

    private boolean mAnswerIsTrue;

    private TextView mShowAnswerTextView;
    private TextView mApiVersionTextView;

    private Button mShowAnswer;

    private Intent data;

    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);

        mShowAnswerTextView = (TextView)findViewById(R.id.answerTextView);

        mShowAnswer = (Button)findViewById(R.id.showAnswerButton);
        mShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setAnswerShownResult(false);

                if(mAnswerIsTrue){
                    mShowAnswerTextView.setText(R.string.true_button);
                }
                else{
                    mShowAnswerTextView.setText(R.string.false_button);
                }
                setAnswerShownResult(true);
            }
        });

        mApiVersionTextView = (TextView)findViewById(R.id.api_version);
        mApiVersionTextView.setText("API Version: " + Build.VERSION.RELEASE);
    }

    private void setAnswerShownResult(boolean isAnswerShown){
        data = new Intent();
        data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
        setResult(RESULT_OK, data);
    }
}
